# advanced_image_viewer
Development of an application to allow common users to easily exploit advanced predictive models for image classification.

## Architecture
![architecture](images/architecture.png)

Architecture composed of two main components:
* **Deep learning framework** to deploy advanced models for image classification (_TensorFlow_)
* **Graphical User Interface** (GUI) to bridge the gap between people and such advanced models (_PySimpleGUIQt_)

The application allows to perform two main tasks.

One is to classify the images with the particular classes of the Imagenet Classification Challenge (ILSVRC-2012-CLS).
The models used for this task were downloaded from _TensorFlow Hub_.

The other is to view and correct the canonical orientation of photos (0 °, 90 °, 180 ° and 270 ° degrees) both manually and automatically.
This is made possible thanks to a predictive model entirely developed by one of us using transfer learning methods in a previous work.


## Execution
In order to launch the application:
* Download source code from https://gitlab.com/vscclh/advanced_image_viewer
* Extract files
* Install necessary dependencies from 'requirements.txt'
* Run _cli.py_

## Wiki
At the following link you can find both user and technical project documentation
https://gitlab.com/vscclh/advanced_image_viewer/-/wikis/home .

## Contributors
* Vasco Coelho 807304
* Miguel Remon 861466
* Alessandro Baio 777746
* Andrea Mauri 795211

## Link to the repository
https://gitlab.com/vscclh/advanced_image_viewer