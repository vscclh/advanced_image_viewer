Module backend
==============

Functions
---------

    
`find_model_dir()`
:   Finds the model folder
    
    Returns
    -------
    srt
        a string containing the path to the model folder

    
`load_and_preprocess_image(img, image_size=256, dynamic_size=False, max_dynamic_size=512)`
:   Loads and preprocesses image to run inference
    
    Parameters
    ----------
    img : PIL Image
        The image to preprocess
    image_size : int
        The image size required by the model
    dynamic_size : bool
        Enabling inference on the unscaled image
    max_dynamic_size : int
        The maximum dynamic input size
    
    Returns
    -------
    tuple of numpy ndarray
        a tuple containing the preprocessed image and the original image

    
`load_model(name)`
:   Loads a TFLite model and allocate tensors
    
    Parameters
    ----------
    name : str
        The file location of the lite model
    
    Returns
    -------
    tensorflow lite.Interpreter
        an interpreter interface for TensorFlow Lite models

    
`predict_angle(interpreter, image)`
:   Predicts canonical orientation of a PIL Image instance
    
    Parameters
    ----------
    interpreter : tensorflow lite.Interpreter
        An interpreter interface for TensorFlow Lite models
    image : PIL Image
        An image to perform inference
    
    Returns
    -------
    str
        a prediction of the class

    
`predict_class(image, model_name='fast (mobilenet_v3_large_100_224)')`
:   Predicts Imagenet (ILSVRC-2012-CLS) class of an image
    
    Parameters
    ----------
    model_name : string
        A string containing the name of the model
    image : PIL Image
        The image to classify
    
    Returns
    -------
    str
        a prediction of the Imagenet (ILSVRC-2012-CLS) class

    
`preprocess_image(image)`
:   Resizes a PIL Image instance to run inference
    
    Parameters
    ----------
    image : PIL Image
        The image to preprocess
    
    Returns
    -------
    PIL Image
        a 224 by 224 PIL Image instance converted to have 3 channels

    
`probs_to_pred(probabilities)`
:   Converts probabilities into a prediction
    
    Parameters
    ----------
    probabilities : list
        A list of probabilities
    
    Returns
    -------
    str
        a prediction of the class

    
`run_inference(interpreter, preprocessed_image)`
:   Runs TFLite inference
    
    Parameters
    ----------
    interpreter : tensorflow lite.Interpreter
        An interpreter interface for TensorFlow Lite models
    preprocessed_image : PIL Image
        A preprocessed image to perform inference
    
    Returns
    -------
    list
        a list of numbers representing the probabilities inferred