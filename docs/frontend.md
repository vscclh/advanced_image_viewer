Module frontend
===============

Functions
---------

    
`auto_rotate_image(image, predicted_angle)`
:   Corrects the orientation of a PIL Image instance
    
    Parameters
    ----------
    image : PIL Image
        The image whose orientation needs to be corrected
    predicted_angle : str
        A string that can be either '90', '180' or '270' degrees
    
    Returns
    -------
    PIL Image
        the image correctly oriented based on prediction

    
`get_file_size_in_bytes(file_path)`
:   Gets the size of a file in bytes
    
    Parameters
    ----------
    file_path : str
        The file location of the file
    
    Returns
    -------
    int
        the total size in bytes

    
`get_image_properties(image, image_path)`
:   Gets basic properties of a PIL Image instance
    
    Parameters
    ----------
    image : PIL Image
        The image whose properties needs to be extracted
    image_path : str
        The file location of the image
    
    Returns
    -------
    dict
        a dict of properties of a PIL Image instance

    
`get_new_image_path(image_path, direction)`
:   Gets the path of the next or previous image in the current folder
    
    Parameters
    ----------
    image_path : str
           Path of the image currently displayed
    direction : bool
       True for the path to the next image, False for the previous one
    
    Returns
    -------
    str
        the desired new image path according to the direction parameter

    
`image_to_byte_array(image)`
:   Saves a PIL Image instance to an in-memory bytes buffer
    
    Parameters
    ----------
    image : PIL Image
        The image to convert to an in-memory bytes buffer
    
    Returns
    -------
    buffer
        the entire contents of a BytesIO object

    
`load_frame_from_video(video, frame_index)`
:   Loads a specific frame from a VideoCapture OpenCV object as a PIL image
    
    Parameters
    ----------
    video : cv2 VideoCapture
        The video we want to load the frame of
    frame_index : int
        The index of the specific frame we want
    
    Returns
    -------
    PIL image
        the specific frame of the video

    
`load_image_from_path(name)`
:   Loads an image into PIL format
    
    Parameters
    ----------
    name : str
        The file location of the image
    
    Returns
    -------
    PIL Image
        the loaded image

    
`load_video_from_path(path)`
:   Loads a video as an openCV VideoCapture object and its middle frame as
    a PIL image
    
    Parameters
    ----------
    path : str
        The file location of the video
    
    Returns
    -------
    dict
        a dict containing the PIL image of the middle frame of the video,
        the VideoCapture OpenCV object of the video,
        the index of the middle frame,
        the amount of frames in the video

    
`message_loaded_image(auto_rotate, predicted_angle)`
:   Selects the right message to show in the UI
    
    Parameters
    ----------
    auto_rotate : bool
        If the automatic orientation correction is enabled
    predicted_angle : str
        A string that can be either '90', '180' or '270' degrees
    
    Returns
    -------
    str
        a message describing the outcome of the automatic correction process

    
`preprocess_exif_data(raw_exif_data)`
:   Preprocesses EXIF metadata of a PIL Image instance
    
    Parameters
    ----------
    raw_exif_data : dict
        A raw dict of EXIF metadata of a PIL Image instance
    
    Returns
    -------
    str
        a string containing EXIF metadata

    
`resize_image(image)`
:   Resizes a PIL Image instance to a thumbnail version of itself
    
    Parameters
    ----------
    image : PIL Image
        The image to thumbnail
    
    Returns
    -------
    PIL Image
        the thumbnail of the image

    
`rotate_image(image, direction)`
:   Rotates a PIL Image instance
    
    Parameters
    ----------
    image : PIL Image
        The image to rotate
    direction : str
        a string used to determine the degree of a counterclockwise rotation
    
    Returns
    -------
    PIL Image
        the image rotated

    
`undo_rotation(image, predicted_angle)`
:   Cancels orientation correction
    
    Parameters
    ----------
    image : PIL Image
        The image whose orientation needs to be canceled
    predicted_angle : str
        A string that can be either '90', '180' or '270' degrees
    
    Returns
    -------
    PIL Image
        the image with the original orientation