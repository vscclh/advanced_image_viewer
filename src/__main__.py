import PySimpleGUIQt as sg
import os

from .frontend import (load_image_from_path, image_to_byte_array,
                       resize_image, rotate_image, auto_rotate_image,
                       message_loaded_image, undo_rotation,
                       get_image_properties, preprocess_exif_data,
                       get_new_image_path, load_video_from_path,
                       load_frame_from_video)

from .backend import (load_model, predict_angle, predict_class)


def update(window, values, variables):
    variables["predicted_angle"] = predict_angle(variables["interpreter"],
                                                 variables["image"])
    variables["auto_rotate"] = values['-AUTO_ROTATE-']
    if variables["auto_rotate"] and variables["predicted_angle"] != '0':
        window['-UNDO-'].update(button_color=('Black', 'Black'),
                                disabled=False)
        variables["image"] = auto_rotate_image(variables["image"],
                                               variables["predicted_angle"])
    else:
        window['-UNDO-'].update(button_color=('Black', 'Grey'),
                                disabled=True),
    msg = message_loaded_image(variables["auto_rotate"],
                               variables["predicted_angle"])
    window['-PREDICTION-'].update(value=msg)
    thumbnail = resize_image(variables["image"].copy())
    window['-IMAGE-'].update(visible=True,
                             data=image_to_byte_array(thumbnail))
    properties = get_image_properties(variables["image"],
                                      variables["image_path"])
    window['-NAME_FILE-'].update(value=properties['filename'])
    window['-FILE_FORMAT-'].update(value=properties['format'])
    window['-IMAGE_MODE-'].update(value=properties['mode'])
    window['-SIZE-'].update(value=properties['size'])
    window['-FILE_SIZE-'].update(value=properties['bytes'])
    try:
        variables["raw_exif_data"] = variables["image"].getexif()
        variables["raw_exif_data"] = preprocess_exif_data(
            variables["raw_exif_data"])
    except AttributeError:
        variables["raw_exif_data"] = None
    temp = []
    for x in variables["raw_exif_data"]:
        temp.append(x)
    if len(temp) == 0:
        variables["raw_exif_data"] = None
    else:
        variables["raw_exif_data"] = temp
    window['-EXIF-'].update(visible=False)
    variables["exif_info_visibility"] = False
    if variables["raw_exif_data"] is not None:
        window['-SHOW_EXIF-'].update(disabled=False,
                                     button_color=('Black',
                                                   'White'))
    else:
        window['-SHOW_EXIF-'].update(disabled=True,
                                     button_color=('Black',
                                                   'Gray'))
    if variables["video_loaded"]:
        window['-SHOW_EXIF-'].update(disabled=True,
                                     button_color=('Black',
                                                   'Gray'))
        window['-FILE_FORMAT-'].update(
            value=variables["image_path"].split('.')[-1])
        window['-VIDEO-'].update(range=(1, variables["video"]["frame_count"]),
                                 disabled=False,
                                 visible=True)
        window['-VIDEO-'].update(value=variables["video"]["frame_index"])
        window['-VIDEO_HEAD-'].update(visible=True)
        window['-VIDEO_FRAME-'].update(visible=True,
                                       disabled=False,
                                       value=variables["video"]["frame_index"])
        msg = "/ " + str(variables["video"]["frame_count"])
        window['-VIDEO_TOT-'].update(visible=True,
                                     value=msg)
        window['-VIDEO_GO-'].update(disabled=False, visible=True)
    else:
        window['-VIDEO-'].update(disabled=True, visible=False)
        window['-VIDEO_HEAD-'].update(visible=False)
        window['-VIDEO_FRAME-'].update(visible=False, disabled=True)
        window['-VIDEO_TOT-'].update(visible=False)
        window['-VIDEO_GO-'].update(disabled=True, visible=False)
    window['-ILSVRC_2012-'].update(disabled=False,
                                   button_color=('Black',
                                                 'White'))
    window['-MODEL-'].update(disabled=False)
    window['-PREVIOUS-'].update(disabled=False,
                                visible=True,
                                button_color=('Black', 'Black'))
    window['-NEXT-'].update(disabled=False,
                            visible=True,
                            button_color=('Black', 'Black'))
    window['-ROTATE_RIGHT-'].update(disabled=False,
                                    visible=True,
                                    button_color=('Black', 'Black'))
    window['-ROTATE_LEFT-'].update(disabled=False,
                                   visible=True,
                                   button_color=('Black', 'Black'))
    return variables


def main():
    model_name = 'lite_rotation_model_vgg16_block5_conv1.tflite'
    variables = {"interpreter": load_model(model_name)}

    cwd = os.getcwd().split('\\')[-1]
    if cwd == 'src' or cwd == 'dist':
        image_dir = '../images/'
    elif cwd == 'cli':
        image_dir = '../../images/'
    elif cwd == 'advanced_image_viewer':
        image_dir = './images/'
    variables["image"] = load_image_from_path(image_dir + 'default.png')

    variables["predicted_angle"] = None
    variables["image_path"] = None
    variables["raw_exif_data"] = None
    variables["exif_info_visibility"] = False
    variables["video"] = None
    variables["video_loaded"] = False
    variables["models"] = ['fast (mobilenet_v3_large_100_224)',
                           'slow (bit_m-r50x1)']

    sg.theme('Black')

    properties_column = [
        [
            sg.Text(
                text='Filename:',
                size_px=(120, 35)),
            sg.Text(
                text='',
                size_px=(140, 35),
                key='-NAME_FILE-'),
            sg.Text(
                text='',
                size_px=(124, 35))
        ],
        [
            sg.Text(
                text='File format:',
                size_px=(120, 35)),
            sg.Text(
                text='',
                size_px=(140, 35),
                key='-FILE_FORMAT-')
        ],
        [
            sg.Text(
                text='Image mode:',
                size_px=(120, 35)),
            sg.Text(
                text='',
                size_px=(140, 35),
                key='-IMAGE_MODE-')
        ],
        [
            sg.Text(
                text='Size (width, height):',
                size_px=(120, 35)),
            sg.Text(
                text='',
                size_px=(140, 35),
                key='-SIZE-')
        ],
        [
            sg.Text(
                text='File size (bytes):',
                size_px=(120, 35)),
            sg.Text(
                text='',
                size_px=(140, 35),
                key='-FILE_SIZE-')
        ]
    ]

    exif_column = [
        [
            sg.Stretch(),
            sg.Text(
                text='',
                size_px=(294, 35)),
            sg.Button(
                button_text='EXIF info',
                enable_events=True,
                button_color=('Black', 'Gray'),
                disabled=True,
                visible=True,
                size_px=(90, 35),
                key='-SHOW_EXIF-')
        ],
        [
            sg.Stretch(),
            sg.Listbox(
                values='',
                visible=False,
                size_px=(380, 120),
                background_color='Black',
                key='-EXIF-')
        ]
    ]

    video_column = [
        [
            sg.Stretch(),
            sg.Slider(
                range=(1, 1000),
                size_px=(280, 50),
                orientation='horizontal',
                disabled=True,
                enable_events=True,
                visible=False,
                key='-VIDEO-'),
            sg.Stretch()
        ],
        [
            sg.Stretch(),
            sg.Text(
                text="Frame: ",
                size_px=(55, 35),
                visible=False,
                key='-VIDEO_HEAD-'
            ),
            sg.Input(
                disabled=True,
                size_px=(90, 35),
                visible=False,
                key='-VIDEO_FRAME-'
            ),
            sg.Text(
                text="",
                size_px=(40, 35),
                visible=False,
                key='-VIDEO_TOT-'
            ),
            sg.Button(
                button_text='Go',
                enable_events=True,
                visible=False,
                size_px=(90, 35),
                key='-VIDEO_GO-'),
            sg.Stretch()
        ]
    ]

    layout = [
        [
            sg.Column([
                [
                    sg.FileBrowse(
                        button_text='Browse',
                        file_types=(('Media', '*.jpg;*.png;*.avi;*.mp4'),
                                    ('Image', '*.jpg;*.png'),
                                    ('Video', '*.avi;*.mp4'),
                                    ('All', "*")),
                        enable_events=True,
                        size=(7, 1),
                        key='-FILE-')
                ]
            ]
            ),
            sg.Column([
                [
                    sg.Button(
                        button_text='Image Classification with TensorFlow Hub',
                        enable_events=True,
                        size_px=(270, 35),
                        button_color=('Black', 'Grey'),
                        disabled=True,
                        visible=True,
                        key='-ILSVRC_2012-'),
                    sg.Text(text='Selected model: '),
                    sg.Combo(values=variables["models"],
                             default_value='fast (mobilenet_v3_large_100_224)',
                             background_color='black',
                             size_px=(230, 30),
                             enable_events=True,
                             key='-MODEL-',
                             disabled=True,
                             readonly=True,
                             visible=True),
                    sg.Text(
                        text='',
                        size_px=(750, 35),
                        key='-RESULT-')
                ]
            ]
            ),
            sg.Stretch()
        ],
        [
            sg.Text(
                text='',
                size_px=(500, 35),
                key='-PREDICTION-'),
            sg.Stretch(),
            sg.Button(
                image_filename=image_dir + 'icons/rotate_left.ico',
                button_color=('Black', 'Grey'),
                enable_events=True,
                size_px=(90, 35),
                disabled=True,
                visible=False,
                key='-ROTATE_LEFT-'
            ),
            sg.Button(
                image_filename=image_dir + 'icons/rotate_right.ico',
                button_color=('Black', 'Grey'),
                enable_events=True,
                size_px=(90, 35),
                disabled=True,
                visible=False,
                key='-ROTATE_RIGHT-'
            ),
            sg.Stretch(),
            sg.Text(
                text='',
                size_px=(220, 35)),
            sg.Checkbox(
                text='Automatic rotation',
                size_px=(140, 35),
                key='-AUTO_ROTATE-'),
            sg.Button(
                image_filename=image_dir + 'icons/undo.ico',
                enable_events=True,
                size_px=(70, 35),
                button_color=('Black', 'Grey'),
                disabled=True,
                visible=True,
                key='-UNDO-'),
            sg.Button(
                image_filename=image_dir + 'icons/save.ico',
                button_color=('Black', 'Black'),
                enable_events=True,
                size_px=(70, 35),
                visible=True,
                key='-SAVE-')
        ],
        [
            sg.Button(
                image_filename=image_dir + 'icons/previous.ico',
                button_color=('Black', 'Grey'),
                enable_events=True,
                size_px=(90, 35),
                disabled=True,
                visible=False,
                key='-PREVIOUS-'),
            sg.Stretch(),
            sg.Image(data=image_to_byte_array(variables["image"]),
                     key='-IMAGE-'),
            sg.Stretch(),
            sg.Button(
                image_filename=image_dir + 'icons/next.ico',
                button_color=('Black', 'Grey'),
                enable_events=True,
                size_px=(90, 35),
                disabled=True,
                visible=False,
                key='-NEXT-')
        ],
        [
            sg.Column(properties_column),
            sg.Stretch(),
            sg.Column(video_column),
            sg.Stretch(),
            sg.Column(exif_column)
        ]
    ]

    window = sg.Window(title='Advanced Image Viewer',
                       layout=layout,
                       location=(0, 0),
                       finalize=True)
    window.maximize()

    while True:
        event, values = window.read()
        print(event, values)

        if event == sg.WIN_CLOSED:
            break

        elif event == '-FILE-':
            if values['-FILE-'][0] != '':
                window['-PREDICTION-'].update(value="Loading..")
                window['-RESULT-'].update(value='')
                window.refresh()

                variables["image_path"] = values['-FILE-'][0]
                extension = variables["image_path"].split('.')[-1]

                if extension in {"mp4", "avi"}:
                    variables["video_loaded"] = True
                    try:
                        variables["video"] = load_video_from_path(
                            variables["image_path"])
                    except ValueError:
                        msg = 'Failed to load Video'
                        window['-PREDICTION-'].update(value=msg)
                        continue
                    variables["image"] = variables["video"]["frame"]
                elif extension in {"jpg", "png"}:
                    variables["video_loaded"] = False
                    variables["image"] = load_image_from_path(
                        variables["image_path"])
                else:
                    msg = 'Unsupported file format'
                    window['-PREDICTION-'].update(value=msg)
                    continue

                variables = update(window, values, variables)

        elif event == '-NEXT-':
            if variables["video_loaded"]:
                if variables["video"]["frame_index"] < \
                        variables["video"]["frame_count"]:
                    variables["image"] = load_frame_from_video(
                        variables["video"]["video"],
                        variables["video"]["frame_index"] + 1)
                    variables["video"]["frame_index"] += 1
                else:
                    continue
            elif variables["image_path"] is not None:
                variables["image_path"] = get_new_image_path(
                    variables["image_path"], direction=True)
                variables["image"] = load_image_from_path(
                    variables["image_path"])
            variables = update(window, values, variables)

        elif event == '-PREVIOUS-':
            if variables["video_loaded"]:
                if variables["video"]["frame_index"] > 1:
                    variables["image"] = load_frame_from_video(
                        variables["video"]["video"],
                        variables["video"]["frame_index"] - 1)
                    variables["video"]["frame_index"] -= 1
                else:
                    continue
            elif variables["image_path"] is not None:
                variables["image_path"] = get_new_image_path(
                    variables["image_path"], direction=False)
                variables["image"] = load_image_from_path(
                    variables["image_path"])
            variables = update(window, values, variables)

        elif event == '-ROTATE_RIGHT-':
            if variables["image"] is not None:
                variables["image"] = rotate_image(variables["image"], 'right')
                msg = 'Image has been manually rotated!'
                window['-PREDICTION-'].update(value=msg)
                window['-UNDO-'].update(button_color=('Black', 'Grey'),
                                        disabled=True)
                thumbnail = resize_image(variables["image"].copy())
                window['-IMAGE-'].update(visible=True,
                                         data=image_to_byte_array(thumbnail))

        elif event == '-ROTATE_LEFT-':
            if variables["image"] is not None:
                variables["image"] = rotate_image(variables["image"], 'left')
                msg = 'Image has been manually rotated!'
                window['-PREDICTION-'].update(value=msg)
                window['-UNDO-'].update(button_color=('Black', 'Grey'),
                                        disabled=True)
                thumbnail = resize_image(variables["image"].copy())
                window['-IMAGE-'].update(visible=True,
                                         data=image_to_byte_array(thumbnail))

        elif event == '-SAVE-':
            if variables["video_loaded"]:
                video_path, video_name = os.path.split(variables["image_path"])
                frame_name = video_name.split('.')[0]
                frame_name += '_f' + str(variables["video"]["frame_index"])
                count = len([name for name in os.listdir(video_path)
                             if name.find(frame_name) == 0])
                msg = 'Frame saved as an image in the video folder'
                window['-PREDICTION-'].update(value=msg)
                print(video_path)
                if count > 0:
                    variables["image"].save(video_path +
                                            '/' +
                                            frame_name +
                                            '(' +
                                            str(count) +
                                            ').png', format='png')
                else:
                    variables["image"].save(video_path +
                                            '/' +
                                            frame_name +
                                            '.png', format='png')
            elif variables["image_path"] is not None:
                msg = 'Image saved!'
                window['-PREDICTION-'].update(value=msg)
                window['-UNDO-'].update(button_color=('Black', 'Grey'),
                                        disabled=True)
                variables["image"].save(variables["image_path"])

        elif event == '-UNDO-':
            if variables["predicted_angle"] is not None and\
                    variables["predicted_angle"] != '0':
                variables["image"] = undo_rotation(
                    variables["image"],
                    variables["predicted_angle"])
                msg = 'This is the original orientation of the loaded image; '
                msg += f'it\'s rotated {variables["predicted_angle"]}'
                msg += '° counterclockwise!'
                window['-PREDICTION-'].update(value=msg)
                window['-UNDO-'].update(button_color=('Black', 'Grey'),
                                        disabled=True)
                thumbnail = resize_image(variables["image"].copy())
                window['-IMAGE-'].update(visible=True,
                                         data=image_to_byte_array(thumbnail))

        elif event == '-SHOW_EXIF-':
            if variables["raw_exif_data"] is not None and \
                    not variables["exif_info_visibility"]:
                print(variables["raw_exif_data"])
                window['-EXIF-'].update(visible=True,
                                        values=variables["raw_exif_data"])
            variables["exif_info_visibility"] = True
        elif variables["raw_exif_data"] is not None and \
                variables["exif_info_visibility"]:
            window['-EXIF-'].update(visible=False)
            variables["exif_info_visibility"] = False

        elif event == '-VIDEO-':
            if variables["video_loaded"]:
                if values['-VIDEO-'] == variables["video"]["frame_index"]:
                    continue
                try:
                    variables["image"] = load_frame_from_video(
                        variables["video"]["video"],
                        values['-VIDEO-'])
                except ValueError:
                    msg = 'Failed to load Frame'
                    window['-PREDICTION-'].update(value=msg)
                    continue
                variables["video"]["frame_index"] = values['-VIDEO-']
                variables = update(window, values, variables)

        elif event == '-VIDEO_GO-':
            if variables["video_loaded"]:
                try:
                    temp = int(values['-VIDEO_FRAME-'])
                except ValueError:
                    msg = 'Please insert a correct value for the frame.'
                    msg += ' Integer between 1 and '
                    msg += str(variables["video"]["frame_count"])
                    window['-PREDICTION-'].update(value=msg)
                    window.refresh()
                    continue
                if temp == variables["video"]["frame_index"]:
                    continue
                try:
                    variables["image"] = load_frame_from_video(
                        variables["video"]["video"], temp)
                except ValueError:
                    msg = 'Please insert a correct value for the frame.'
                    msg += ' Integer between 1 and '
                    msg += str(variables["video"]["frame_count"])
                    window['-PREDICTION-'].update(value=msg)
                    window.refresh()
                    continue
                except TypeError:
                    msg = 'Please insert a correct value for the frame.'
                    msg += ' Integer between 1 and '
                    msg += str(variables["video"]["frame_count"])
                    window['-PREDICTION-'].update(value=msg)
                    window.refresh()
                    continue
                variables["video"]["frame_index"] = temp
                variables = update(window, values, variables)

        elif event == '-ILSVRC_2012-':
            if values['-MODEL-'] in variables["models"]:
                window['-RESULT-'].update(value='Running inference..')
                window.refresh()
                model = values['-MODEL-']
                predicted_class = predict_class(variables["image"],
                                                model)
                window['-RESULT-'].update(value=predicted_class)
            else:
                msg = 'Please select a correct model'
                window['-PREDICTION-'].update(value=msg)
                window.refresh()
                continue

    window.close()


if __name__ == '__main__':
    main()
