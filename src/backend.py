from PIL import Image
import tensorflow as tf
import numpy as np
import os


def preprocess_image(image):
    """Resizes a PIL Image instance to run inference

    Parameters
    ----------
    image : PIL Image
        The image to preprocess

    Returns
    -------
    PIL Image
        a 224 by 224 PIL Image instance converted to have 3 channels
    """

    new_dim = (224, 224)
    resample = Image.BICUBIC
    resized_image = image.resize(size=new_dim, resample=resample, box=None)
    if resized_image.mode != 'RGB':  # images are converted to have 3 channels
        resized_image = resized_image.convert('RGB')
    return resized_image


def find_model_dir():
    """Finds the model folder

    Returns
    -------
    srt
        a string containing the path to the model folder
    """

    cwd = os.getcwd().split('\\')[-1]
    model_dir = 'models/'
    if cwd == 'src' or cwd == 'dist':
        model_dir = '../models/'
    elif cwd == 'cli':
        model_dir = '../../models/'
    elif cwd == 'advanced_image_viewer':
        model_dir = './models/'
    return model_dir


def load_model(name):
    """Loads a TFLite model and allocate tensors

    Parameters
    ----------
    name : str
        The file location of the lite model

    Returns
    -------
    tensorflow lite.Interpreter
        an interpreter interface for TensorFlow Lite models
    """

    model_dir = find_model_dir()
    interpreter = tf.lite.Interpreter(model_path=model_dir + name)
    interpreter.allocate_tensors()
    return interpreter


def run_inference(interpreter, preprocessed_image):
    """Runs TFLite inference

    Parameters
    ----------
    interpreter : tensorflow lite.Interpreter
        An interpreter interface for TensorFlow Lite models
    preprocessed_image : PIL Image
        A preprocessed image to perform inference

    Returns
    -------
    list
        a list of numbers representing the probabilities inferred
    """

    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    input_data = tf.keras.preprocessing.image.img_to_array(preprocessed_image)
    input_data = np.array([input_data])
    interpreter.set_tensor(input_details[0]['index'], input_data)
    interpreter.invoke()
    output_data = interpreter.get_tensor(output_details[0]['index'])
    return output_data


def probs_to_pred(probabilities):
    """Converts probabilities into a prediction

    Parameters
    ----------
    probabilities : list
        A list of probabilities

    Returns
    -------
    str
        a prediction of the class
    """

    y_lite = np.argmax(probabilities)
    class_names = ['0', '180', '270', '90']
    predicted_angle = class_names[y_lite]
    return predicted_angle


def predict_angle(interpreter, image):
    """Predicts canonical orientation of a PIL Image instance

    Parameters
    ----------
    interpreter : tensorflow lite.Interpreter
        An interpreter interface for TensorFlow Lite models
    image : PIL Image
        An image to perform inference

    Returns
    -------
    str
        a prediction of the class
    """

    preprocessed_image = preprocess_image(image)
    output_data = run_inference(interpreter, preprocessed_image)
    probabilities = output_data[0]
    predicted_angle = probs_to_pred(probabilities)
    return predicted_angle


def load_and_preprocess_image(img,
                              image_size=256,
                              dynamic_size=False,
                              max_dynamic_size=512):
    """Loads and preprocesses image to run inference

    Parameters
    ----------
    img : PIL Image
        The image to preprocess
    image_size : int
        The image size required by the model
    dynamic_size : bool
        Enabling inference on the unscaled image
    max_dynamic_size : int
        The maximum dynamic input size

    Returns
    -------
    tuple of numpy ndarray
        a tuple containing the preprocessed image and the original image
    """

    img_raw = img

    if img.mode != 'RGB':  # images are converted to have 3 channels
        img = img.convert('RGB')

    img = np.array(img)
    # Reshape into shape [batch_size, height, width, num_channels]
    img_reshaped = tf.reshape(img,
                              [1, img.shape[0], img.shape[1], img.shape[2]])
    # Use 'convert_image_dtype' to convert to floats in the [0,1] range.
    img = tf.image.convert_image_dtype(img_reshaped, tf.float32)

    # Load and convert to float32 numpy array,
    # add batch dimension and normalize to range [0, 1].
    if tf.reduce_max(img) > 1.0:
        img = img / 255.
    if len(img.shape) == 3:
        img = tf.stack([img, img, img], axis=-1)
    if not dynamic_size:
        img = tf.image.resize_with_pad(img, image_size, image_size)
    elif img.shape[1] > max_dynamic_size or img.shape[2] > max_dynamic_size:
        img = tf.image.resize_with_pad(img, max_dynamic_size, max_dynamic_size)
    return img, img_raw


def predict_class(image, model_name='fast (mobilenet_v3_large_100_224)'):
    """Predicts Imagenet (ILSVRC-2012-CLS) class of an image

    Parameters
    ----------
    model_name : string
        A string containing the name of the model
    image : PIL Image
        The image to classify

    Returns
    -------
    str
        a prediction of the Imagenet (ILSVRC-2012-CLS) class
    """

    model_dir = find_model_dir()
    image_size = 224

    model_handle_map = {
        'slow (bit_m-r50x1)':
            model_dir + 'bit_m-r50x1_ilsvrc2012_classification_1',
        'fast (mobilenet_v3_large_100_224)':
            model_dir + 'imagenet_mobilenet_v3_large_100_224_classification_5'
    }
    model_image_size_map = {
        'fast (mobilenet_v3_large_100_224)': 224
    }
    model_handle = model_handle_map[model_name]

    max_dynamic_size = 512
    if model_name in model_image_size_map:
        image_size = model_image_size_map[model_name]
        dynamic_size = False
    else:
        dynamic_size = True

    i = 0
    with open(model_dir + 'ImageNetLabels.txt') as f:
        labels = f.readlines()
        classes = [label.strip() for label in labels[1:]]
        i += 1

    image, original_image = load_and_preprocess_image(image,
                                                      image_size,
                                                      dynamic_size,
                                                      max_dynamic_size)
    classifier = tf.saved_model.load(model_handle)
    probabilities = tf.nn.softmax(classifier(image)).numpy()
    top_5 = tf.argsort(probabilities,
                       axis=-1,
                       direction='DESCENDING')[0][:5].numpy()

    # Some models include an additional 'background' class in the predictions,
    # so we must account for this when reading the class labels.
    includes_background_class = probabilities.shape[1] == 1001

    result = 'Top 5: '
    for i, item in enumerate(top_5):
        class_index = item if not includes_background_class else item - 1
        line = f'{classes[class_index]}:' \
               f'{"{:.2%}".format(probabilities[0][top_5][i])}'
        if i != 4:
            result += line + ', '
        else:
            result += line

    return result
