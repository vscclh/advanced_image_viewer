from PIL import Image
from PIL.ExifTags import TAGS
import cv2
import io
import os


def load_image_from_path(name):
    """Loads an image into PIL format

    Parameters
    ----------
    name : str
        The file location of the image

    Returns
    -------
    PIL Image
        the loaded image
    """

    image = Image.open(name, mode='r')
    return image


def image_to_byte_array(image):
    """Saves a PIL Image instance to an in-memory bytes buffer

    Parameters
    ----------
    image : PIL Image
        The image to convert to an in-memory bytes buffer

    Returns
    -------
    buffer
        the entire contents of a BytesIO object
    """

    bio = io.BytesIO()
    image.save(bio, format='png')
    del image
    return bio.getvalue()


def rotate_image(image, direction):
    """Rotates a PIL Image instance

    Parameters
    ----------
    image : PIL Image
        The image to rotate
    direction : str
        a string used to determine the degree of a counterclockwise rotation

    Returns
    -------
    PIL Image
        the image rotated
    """

    rotate = [Image.ROTATE_90, Image.ROTATE_270, Image.ROTATE_180]
    if (image is not None) and (direction in ('left', 'right', 'upside_down')):
        if direction == 'left':
            image = image.transpose(method=rotate[0])
        elif direction == 'right':
            image = image.transpose(method=rotate[1])
        else:
            image = image.transpose(method=rotate[2])
    return image


def resize_image(image):
    """Resizes a PIL Image instance to a thumbnail version of itself

    Parameters
    ----------
    image : PIL Image
        The image to thumbnail

    Returns
    -------
    PIL Image
        the thumbnail of the image
    """

    width, height = image.size
    size = 1280, 720
    if width > size[0] or height > size[1]:
        image.thumbnail(size)
    return image


def auto_rotate_image(image, predicted_angle):
    """Corrects the orientation of a PIL Image instance

    Parameters
    ----------
    image : PIL Image
        The image whose orientation needs to be corrected
    predicted_angle : str
        A string that can be either '90', '180' or '270' degrees

    Returns
    -------
    PIL Image
        the image correctly oriented based on prediction
    """

    if predicted_angle == '90':
        image = rotate_image(image, 'right')
    elif predicted_angle == '180':
        image = rotate_image(image, 'upside_down')
    elif predicted_angle == '270':
        image = rotate_image(image, 'left')
    return image


def message_loaded_image(auto_rotate, predicted_angle):
    """Selects the right message to show in the UI

    Parameters
    ----------
    auto_rotate : bool
        If the automatic orientation correction is enabled
    predicted_angle : str
        A string that can be either '90', '180' or '270' degrees

    Returns
    -------
    str
        a message describing the outcome of the automatic correction process
    """

    if auto_rotate:
        if predicted_angle == '0':
            msg = 'Loaded image was already correctly oriented!'
        else:
            msg = (f'Loaded image was rotated {predicted_angle}° '
                   'counterclockwise! Now it\'s correct!')
    elif predicted_angle == '0':
        msg = 'Loaded image is correctly oriented!'
    else:
        msg = f'Loaded image is rotated {predicted_angle}° counterclockwise!'
    return msg


def undo_rotation(image, predicted_angle):
    """Cancels orientation correction

    Parameters
    ----------
    image : PIL Image
        The image whose orientation needs to be canceled
    predicted_angle : str
        A string that can be either '90', '180' or '270' degrees

    Returns
    -------
    PIL Image
        the image with the original orientation
    """

    undo_angle = str((360 - int(predicted_angle)) % 360)
    image = auto_rotate_image(image, undo_angle)
    return image


def get_file_size_in_bytes(file_path):
    """Gets the size of a file in bytes

    Parameters
    ----------
    file_path : str
        The file location of the file

    Returns
    -------
    int
        the total size in bytes
    """

    stat_info = os.stat(file_path)
    size = stat_info.st_size
    return size


def get_image_properties(image, image_path):
    """Gets basic properties of a PIL Image instance

    Parameters
    ----------
    image : PIL Image
        The image whose properties needs to be extracted
    image_path : str
        The file location of the image

    Returns
    -------
    dict
        a dict of properties of a PIL Image instance
    """

    properties = {'filename': os.path.basename(image_path).split('.')[0],
                  'format': image.format,
                  'mode': image.mode,
                  'size': image.size,
                  'bytes': get_file_size_in_bytes(image_path)}
    return properties


def preprocess_exif_data(raw_exif_data):
    """Preprocesses EXIF metadata of a PIL Image instance

     Parameters
     ----------
     raw_exif_data : dict
         A raw dict of EXIF metadata of a PIL Image instance

     Returns
     -------
     str
         a string containing EXIF metadata
     """

    exif = {
        TAGS[tag]: value
        for tag, value in raw_exif_data.items()
        if tag in TAGS
    }

    selected_tags = ['ApertureValue',
                     'Artist',
                     'BodySerialNumber',
                     'Copyright',
                     'DateTime',
                     'ExposureTime',
                     'Make',
                     'MaxApertureValue',
                     'Model',
                     'Software']

    exif_subset = {
        tag: value
        for tag, value in exif.items()
        if tag in selected_tags
    }

    exif_data = (key +
                 ': ' +
                 str(val.decode('UTF-8')
                     if isinstance(val, bytes)
                     else val).split('\x00', 1)[0]
                 for key, val in sorted(exif_subset.items(),
                                        key=lambda x: x[0],
                                        reverse=False))
    return exif_data


def get_new_image_path(image_path, direction):
    """Gets the path of the next or previous image in the current folder

     Parameters
     ----------
     image_path : str
            Path of the image currently displayed
     direction : bool
        True for the path to the next image, False for the previous one

     Returns
     -------
     str
         the desired new image path according to the direction parameter
     """

    current_dir = os.path.dirname(image_path)
    valid_extensions = ['.jpg', '.png', '.bmp']
    image_paths = []
    for filename in os.listdir(current_dir):
        extensions = os.path.splitext(filename)[1]
        if extensions.lower() not in valid_extensions:
            continue
        image_paths.append(current_dir + '/' + filename)
    number_of_images = len(image_paths)
    new_image_path = None
    for index, path in enumerate(image_paths):
        if path == image_path:
            if direction is True:
                new_image_path = image_paths[(index + 1) % number_of_images]
            else:
                new_image_path = image_paths[(index - 1) % number_of_images]
    return new_image_path


def load_video_from_path(path):
    """Loads a video as an openCV VideoCapture object and its middle frame as
    a PIL image

    Parameters
    ----------
    path : str
        The file location of the video

    Returns
    -------
    dict
        a dict containing the PIL image of the middle frame of the video,
        the VideoCapture OpenCV object of the video,
        the index of the middle frame,
        the amount of frames in the video
    """
    video = cv2.VideoCapture(path)

    count = int(video.get(cv2.CAP_PROP_FRAME_COUNT))

    frame_index = int(count/2)

    frame = load_frame_from_video(video, frame_index)

    return {"frame": frame,
            "video": video,
            "frame_index": frame_index,
            "frame_count": count}


def load_frame_from_video(video, frame_index):
    """Loads a specific frame from a VideoCapture OpenCV object as a PIL image

        Parameters
        ----------
        video : cv2 VideoCapture
            The video we want to load the frame of
        frame_index : int
            The index of the specific frame we want

        Returns
        -------
        PIL image
            the specific frame of the video
        """
    video.set(cv2.CAP_PROP_POS_FRAMES, frame_index - 1)
    check, frame = video.read()
    if check:
        frame = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
    else:
        raise ValueError('Frame index out of range')
    return frame
