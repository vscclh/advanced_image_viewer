import numpy as np
import tensorflow as tf

from PIL import Image
# import requests
# from io import BytesIO

from time import time

print('TF version:', tf.__version__)
print('GPU: ', tf.config.list_physical_devices('GPU'))


# Load the TFLite model and allocate tensors.
model_dir = r'models/'
model_name = r'lite_rotation_model_vgg16_block5_conv1.tflite'

interpreter = tf.lite.Interpreter(model_path=model_dir+model_name)
interpreter.allocate_tensors()

# Get input and output tensors.
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

# Test the model on random input data.
input_shape = input_details[0]['shape']


def run_inference(image):
    # converts a PIL Image instance to a Numpy array
    input_data = tf.keras.preprocessing.image.img_to_array(image)
    # reshape into shape [batch_size, height, width, num_channels]
    input_data = np.array([input_data])
    interpreter.set_tensor(input_details[0]['index'], input_data)
    interpreter.invoke()
    # The function `get_tensor()` returns a copy of the tensor data.
    # Use `tensor()` in order to get a pointer to the tensor.
    output_data = interpreter.get_tensor(output_details[0]['index'])
    return output_data


NEW_WIDTH = 224
NEW_HEIGHT = 224
NEW_DIM = (NEW_WIDTH, NEW_HEIGHT)
RESAMPLE = Image.BICUBIC
# to manually rotate the image
ROTATE = [Image.ROTATE_90, Image.ROTATE_180, Image.ROTATE_270]

'''
def load_image_from_url(url):
  'Loads an image into PIL format.'
  response = requests.get(url)
  image = Image.open(BytesIO(response.content))
  return image
'''


def load_image_from_path(path):
    'Loads an image into PIL format.'
    image = Image.open(path, mode='r')
    return image


def preprocess_image(image):
    'Converts a PIL Image instance to a Numpy array.'
    resized_image = image.resize(size=NEW_DIM, resample=RESAMPLE, box=None)
    if resized_image.mode != 'RGB':  # images are converted to have 3 channels
        resized_image = resized_image.convert('RGB')
    # Uncomment the next line of code to optionally rotate the image
    # resized_image = resized_image.transpose(method=ROTATE[1])
    return resized_image


# url = ''
# original_image = load_image_from_url(url)
# preprocessed_image = preprocess_image(original_image)

image_dir = r'images/'
image_name = r'lake.jpg'
original_image = load_image_from_path(image_dir+image_name)
preprocessed_image = preprocess_image(original_image)

'''
import matplotlib.pyplot as plt

plt.figure(figsize=(7, 7))
plt.imshow(preprocessed_image)
plt.axis('off')
plt.show()
'''

CLASS_NAMES = ['0', '90', '180', '270']
int_to_str_labels = {}
for i, k in enumerate(CLASS_NAMES):
    int_to_str_labels.update({i: k})


t0 = time()
probs_lite = run_inference(preprocessed_image)[0]
t1 = time()
print('Inference completed in %0.3f seconds!' % (t1 - t0))

print(probs_lite)
y_lite = np.argmax(probs_lite)
predicted_angle = int_to_str_labels[y_lite]
msg = f'The given photo is rotated {predicted_angle} degrees counterclockwise!'
print(msg)
