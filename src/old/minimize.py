'''
Deployment of the original model 'rotation_model_vgg16_block5_conv1.h5'
to TF Lite.

Post-training quantization is a conversion technique that can reduce model size
while also improving CPU and hardware accelerator latency,
with little degradation in model accuracy.

Dynamic range quantization:
The simplest form of post-training quantization statically quantizes only
the weights from floating point to integer, which has 8-bits of precision.
At inference, weights are converted from 8-bits of precision
to floating point and computed using floating-point kernels.
This conversion is done once and cached to reduce latency.
To further improve latency, "dynamic-range" operators dynamically quantiz
activations based on their range to 8-bits
and perform computations with 8-bit weights and activations.
This optimization provides latencies close to fully fixed-point inference.
However, the outputs are still stored using floating point
so that the speedup with dynamic-range ops
is less than a full fixed-point computation.
'''

# Load model and architecture from path.
import tensorflow as tf
from tensorflow.keras.models import load_model

model_dir = 'models/'
model_name = 'vgg16_block5_conv1'
model = load_model(model_dir+f'rotation_model_{model_name}.h5')
model.summary()

# Dynamic range quantization.
converter = tf.lite.TFLiteConverter.from_keras_model(model)
optimize_lite_model = True
if optimize_lite_model:
    converter.optimizations = [tf.lite.Optimize.DEFAULT]
lite_model_content = converter.convert()

# Save the converted model.
with open(f'lite_rotation_model_{model_name}%s.tflite' %
          '_optimized' if optimize_lite_model else '', 'wb') as f:
    f.write(lite_model_content)
print('Wrote %sTFLite model of %d bytes.' %
      ('optimized ' if optimize_lite_model else '',
       len(lite_model_content)))
