import os
import shutil

spec_file = 'cli.spec'
if os.path.exists(spec_file):
    os.remove(spec_file)

build_dir = 'build'
if os.path.exists(build_dir):
    shutil.rmtree(build_dir)

dist_dir = 'dist'
if os.path.exists(dist_dir):
    shutil.rmtree(dist_dir)

command = 'pyinstaller -w --clean --log-level CRITICAL cli.py'
res = os.system(command)
