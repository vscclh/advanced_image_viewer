import os
import shutil
import subprocess

path = os.getcwd()
cwd = path.split('\\')[-1]

while cwd != 'advanced_image_viewer' and path != '':
    path = path[:len(path)-len(cwd)-1]
    cwd = path.split('\\')[-1]

if path == '':
    raise Exception('Working directory is not valid.')

os.chdir(path)

folder = 'build'
try:
    shutil.rmtree(os.path.join(path, folder))
except FileNotFoundError:
    pass

folder = 'dist'
try:
    shutil.rmtree(os.path.join(path, folder))
except FileNotFoundError:
    pass

cmd = r'pyinstaller -w --clean --log-level CRITICAL cli.py'
build = subprocess.Popen(cmd)
build.wait()
build.terminate()

cmd = r'python -m unittest discover -s tests/system'
res = os.system(cmd)
