import unittest
import subprocess
import pyautogui as p


class TestGUI(unittest.TestCase):

    def test_manage_multiple_video_frames(self):
        image_dir = r'tests\fixtures\images' + '\\'
        util_dir = r'tests\fixtures\utils' + '\\'
        video_dir = r'tests\fixtures\video' + '\\'

        gui = subprocess.Popen(r'start dist\cli\cli.exe', shell=True)

        image_location = None
        i = 0
        while image_location is None and i < 20:
            image_location = p.locateOnScreen(util_dir + 'start.png')
            p.sleep(1)
            i = i + 1
        if i == 20 and image_location is None:
            gui.terminate()
            raise Exception("Timeout: failed to start the application")

        p.click(x=util_dir + 'browse.png', duration=0.5)
        p.sleep(0.5)

        p.write(video_dir + 'seal_hd.mp4', interval=0.05)
        p.sleep(0.5)
        p.press('enter')
        p.sleep(0.5)

        image_location = 42
        i = 0
        while image_location is not None and i < 110:
            image_location = p.locateOnScreen(util_dir + 'loading.png')
            p.sleep(1)
            i = i + 1
        if i == 110 and image_location is not None:
            p.hotkey('alt', 'f4')
            gui.terminate()
            raise Exception("Timeout: failed to load and process the image")

        image_location = p.locateOnScreen(image_dir +
                                          'seal_hd_f122_thumbnail.png')
        try:
            self.assertIsNotNone(image_location)
        except AssertionError:
            p.hotkey('alt', 'f4')
            gui.terminate()
            raise

        x, y = p.locateCenterOnScreen(util_dir + 'slider.png')
        x1, y1 = p.locateCenterOnScreen(util_dir + 'frame.png')
        p.moveTo(x, y, 0.5)
        p.dragTo(x+50, y, 15, button='left')
        p.sleep(3)

        image_location = p.locateOnScreen(image_dir +
                                          'seal_hd_f167_thumbnail.png')
        try:
            self.assertIsNotNone(image_location)
        except AssertionError:
            p.hotkey('alt', 'f4')
            gui.terminate()
            raise

        p.click(x1, y1, duration=0.5, clicks=3)
        p.write('1')
        p.click(x=util_dir + 'go.png', duration=0.5)
        p.sleep(1)

        image_location = p.locateOnScreen(image_dir +
                                          'seal_hd_f1_thumbnail.png')
        try:
            self.assertIsNotNone(image_location)
        except AssertionError:
            p.hotkey('alt', 'f4')
            gui.terminate()
            raise

        p.click(x=util_dir + 'automatic_rotation.png', duration=0.5)
        p.click(x=util_dir + 'browse.png', duration=0.5)
        p.sleep(0.5)

        p.write(video_dir + 'seal180.mp4', interval=0.05)
        p.sleep(0.5)
        p.press('enter')
        p.sleep(2)

        image_location = p.locateOnScreen(image_dir +
                                          'seal180_f122_auto_thumbnail.png')
        try:
            self.assertIsNotNone(image_location)
        except AssertionError:
            p.hotkey('alt', 'f4')
            gui.terminate()
            raise

        image_location = p.locateCenterOnScreen(util_dir +
                                                'message_auto180.png')
        try:
            assert image_location is not None
        except AssertionError:
            p.hotkey('alt', 'f4')
            gui.terminate()
            raise

        p.moveTo(x=image_location[0],
                 y=image_location[1],
                 duration=0.5)
        p.sleep(0.5)

        p.click(x=util_dir + 'undo.png', duration=0.5)
        p.sleep(0.5)
        image_location = p.locateOnScreen(image_dir +
                                          'seal180_f122_thumbnail.png')
        try:
            self.assertIsNotNone(image_location)
        except AssertionError:
            p.hotkey('alt', 'f4')
            gui.terminate()
            raise

        p.click(x=util_dir + 'next.png', duration=0.5)
        p.sleep(1)
        image_location = p.locateOnScreen(image_dir +
                                          'seal180_f123_auto_thumbnail.png')
        try:
            self.assertIsNotNone(image_location)
        except AssertionError:
            p.hotkey('alt', 'f4')
            gui.terminate()
            raise

        p.click(x=util_dir + 'exit.png',
                duration=0.5)

        gui.terminate()


if __name__ == '__main__':
    unittest.main()
