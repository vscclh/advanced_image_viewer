import unittest
import subprocess
import pyautogui as p


class TestGUI(unittest.TestCase):

    def test_show_basic_image_properties(self):
        image_dir = r'tests\fixtures\images' + '\\'
        util_dir = r'tests\fixtures\utils' + '\\'

        gui = subprocess.Popen(r'start dist\cli\cli.exe', shell=True)

        image_location = None
        i = 0
        while image_location is None and i < 20:
            image_location = p.locateOnScreen(util_dir + 'start.png')
            p.sleep(1)
            i = i + 1
        if i == 20 and image_location is None:
            gui.terminate()
            raise Exception("Timeout: failed to start the application")

        image_location = p.locateCenterOnScreen(util_dir +
                                                'basic_properties.png')
        try:
            self.assertIsNotNone(image_location)
        except AssertionError:
            p.hotkey('alt', 'f4')
            gui.terminate()
            raise

        p.moveTo(x=image_location[0],
                 y=image_location[1],
                 duration=0.5)
        p.sleep(0.5)

        p.click(x=util_dir + 'browse.png',
                duration=0.5)
        p.sleep(0.5)

        p.write(image_dir + 'tiger.jpg', interval=0.05)
        p.sleep(0.5)
        p.press('enter')
        p.sleep(0.5)

        image_location = 42
        i = 0
        while image_location is not None and i < 110:
            image_location = p.locateOnScreen(util_dir + 'loading.png')
            p.sleep(1)
            i = i + 1
        if i == 110 and image_location is not None:
            p.hotkey('alt', 'f4')
            gui.terminate()
            raise Exception("Timeout: failed to load and process the image")

        image_location = p.locateCenterOnScreen(util_dir +
                                                'basic_properties1.png')
        try:
            self.assertIsNotNone(image_location)
        except AssertionError:
            p.hotkey('alt', 'f4')
            gui.terminate()
            raise

        p.moveTo(x=image_location[0],
                 y=image_location[1],
                 duration=0.5)
        p.sleep(0.5)

        p.click(x=util_dir + 'exit.png',
                duration=0.5)

        gui.terminate()


if __name__ == '__main__':
    unittest.main()
