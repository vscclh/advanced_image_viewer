import re
import os
import unittest

from src import (load_image_from_path, image_to_byte_array, resize_image,
                 load_model, run_inference, probs_to_pred, preprocess_image,
                 rotate_image, auto_rotate_image, message_loaded_image,
                 undo_rotation, predict_angle, get_image_properties,
                 preprocess_exif_data, get_new_image_path,
                 find_model_dir, load_and_preprocess_image, predict_class,
                 load_video_from_path, load_frame_from_video)


class TestGUI(unittest.TestCase):

    def test_load_image_from_path(self):
        path = r'tests/fixtures/images/lake.jpg'
        img = load_image_from_path(path)
        self.assertIsNotNone(img)
        img.close()

    def test_image_to_byte_array(self):
        path = r'tests/fixtures/images/lake.jpg'
        img = load_image_from_path(path)
        arr = image_to_byte_array(img)
        self.assertIsNotNone(arr)
        img.close()

    def test_resize_image(self):
        path = r'tests/fixtures/images/lake.jpg'
        img = load_image_from_path(path)
        original_size = img.size
        resize_image(img)
        if max(original_size) >= 1280:
            self.assertEqual(max(img.size), 1280)
        else:
            self.assertEqual(max(img.size), max(original_size))
        img.close()

    def test_preprocess_image(self):
        path = r'tests/fixtures/images/lake.jpg'
        img = load_image_from_path(path)
        preprocessed_image = preprocess_image(img)
        self.assertIsNotNone(preprocessed_image)
        img.close()

    def test_load_model(self):
        model_path = 'lite_rotation_model_vgg16_block5_conv1.tflite'
        interpreter = load_model(model_path)
        self.assertIsNotNone(interpreter)

    def test_run_inference(self):
        model_path = 'lite_rotation_model_vgg16_block5_conv1.tflite'
        interpreter = load_model(model_path)
        path = r'tests/fixtures/images/tiger.jpg'
        img = load_image_from_path(path)
        preprocessed_image = preprocess_image(img)
        probs = run_inference(interpreter, preprocessed_image)[0]
        predicted_angle = probs_to_pred(probs)
        self.assertEqual(predicted_angle, '0')
        img.close()

    def test_probs_to_pred(self):
        probs = [0.3, 0.2, 0.1, 0.4]
        predicted_angle = probs_to_pred(probs)
        self.assertEqual(predicted_angle, '90')

    def test_predict_angle(self):
        path = r'tests/fixtures/images/lake.jpg'
        img = load_image_from_path(path)
        path = r'lite_rotation_model_vgg16_block5_conv1.tflite'
        interpreter = load_model(path)
        predicted_angle = predict_angle(interpreter, img)
        self.assertTrue(predicted_angle in ('0', '180', '270', '90'))
        img.close()

    def test_rotate_image(self):
        path = r'tests/fixtures/images/lake.jpg'
        img = load_image_from_path(path)
        (original_width, original_height) = img.size
        rotated_img = rotate_image(img, 'right')
        new_width, new_height = rotated_img.size
        self.assertEqual(original_width, new_height)
        self.assertEqual(original_height, new_width)
        img.close()

    def test_auto_rotate_image(self):
        path = r'tests/fixtures/images/lake.jpg'
        img = load_image_from_path(path)
        img = rotate_image(img, 'right')
        self_fixed_img = rotate_image(img, 'left')
        predicted_angle = '270'
        fixed_img = auto_rotate_image(img, predicted_angle)
        self.assertEqual(self_fixed_img, fixed_img)
        img.close()

    def test_message_loaded_image(self):
        predicted_angle = '0'
        self.assertEqual(message_loaded_image(True, predicted_angle),
                         'Loaded image was already correctly oriented!')
        self.assertEqual(message_loaded_image(False, predicted_angle),
                         'Loaded image is correctly oriented!')
        predicted_angle = '270'
        self.assertEqual(message_loaded_image(True, predicted_angle),
                         (f'Loaded image was rotated {predicted_angle}° '
                          'counterclockwise! Now it\'s correct!'))
        self.assertEqual(message_loaded_image(False, predicted_angle),
                         (f'Loaded image is rotated {predicted_angle}° '
                          'counterclockwise!'))

    def test_undo_rotation(self):
        path = r'tests/fixtures/images/lake.jpg'
        img = load_image_from_path(path)
        predicted_angle = '180'
        img = auto_rotate_image(img, predicted_angle)
        manual_undo = rotate_image(img, 'upside_down')
        img = undo_rotation(img, predicted_angle)
        self.assertEqual(img, manual_undo)
        img.close()

    def test_image_properties(self):
        path = r'tests/fixtures/images/lake.jpg'
        img = load_image_from_path(path)
        img_name = 'lake'
        img_file_format = 'JPEG'
        img_mode = 'RGB'
        img_size = (1920, 1080)
        img_size_bytes = 1679421
        properties = get_image_properties(img, path)
        self.assertEqual(properties['filename'], img_name),
        self.assertEqual(properties['format'], img_file_format),
        self.assertEqual(properties['mode'], img_mode),
        self.assertEqual(properties['size'], img_size),
        self.assertEqual(properties['bytes'], img_size_bytes)
        img.close()

    def test_preprocess_exif_data(self):
        path = r'tests/fixtures/images/building.jpg'
        img = load_image_from_path(path)
        try:
            raw_exif_data = img.getexif()
        except AttributeError:
            raw_exif_data = None
        if raw_exif_data is not None:
            metadata = preprocess_exif_data(raw_exif_data)
            self.assertIsNotNone(metadata)
        img.close()

    def test_get_new_image_path(self):
        path = r'tests/fixtures/images/slideshow/lake.png'
        next_image_path = get_new_image_path(path, direction=True)
        self.assertEqual(re.sub(r'/', r'\\', next_image_path),
                         r'tests\fixtures\images\slideshow\park.png')
        previous_image_path = get_new_image_path(path, direction=False)
        self.assertEqual(re.sub(r'/', r'\\', previous_image_path),
                         r'tests\fixtures\images\slideshow\building.png')

    def test_find_model_dir(self):
        cwd = os.getcwd().split('\\')[-1]
        model_dir = 'models/'
        if cwd == 'advanced_image_viewer':
            model_dir = './models/'
        self.assertEqual(find_model_dir(), model_dir)

    def test_load_and_preprocess_image(self):
        path = r'tests/fixtures/images/lake.jpg'
        img = load_image_from_path(path)
        img, original_img = load_and_preprocess_image(img)
        self.assertEqual(img.shape[1], 256)
        self.assertEqual(img.shape[2], 256)

    def test_predict_class(self):
        path = r'tests/fixtures/images/lake.jpg'
        img = load_image_from_path(path)
        top_5 = predict_class(img,
                              model_name='fast (mobilenet_v3_large_100_224)')
        self.assertNotEqual(top_5, 'Top 5: ')

    def test_load_video_from_path(self):
        path = r'tests/fixtures/video/seal_hd.mp4'
        video = load_video_from_path(path)
        self.assertIsNotNone(video["frame"])
        self.assertIsNotNone(video["video"])
        self.assertEqual(video["frame_index"], 122)
        self.assertEqual(video["frame_count"], 245)

    def test_load_frame_from_video(self):
        path = r'tests/fixtures/video/seal_hd.mp4'
        video = load_video_from_path(path)
        frame = load_frame_from_video(video["video"], 1)
        self.assertIsNotNone(frame)
        frame122 = load_frame_from_video(video["video"], 122)
        self.assertEqual(video["frame"], frame122)


if __name__ == '__main__':
    unittest.main()
